import os
import time
import shutil
import wget
import pathlib
import my_module_logs_write as logs
import pandas as pd

import subprocess as sp

import my_module_get_snmp as snmp
import my_module_string_CUT as tof
# добавление класса декоратора для контроля времени выполнения
from my_class_decor import elapsed_time_deco as etd
#import my_module_group_BU as group      # УБРАТЬ

from datetime import datetime as dt

#
# ------------------------- ПЕРЕМЕННЫЕ----------------------
df_r0 = pd.DataFrame() # пустой фрейм для списка роутером
df_s0 = pd.DataFrame() # пустой фрейм для списка коммутаторов
list_sw300= []      # список для свича 300
list_sw2960 = []    # список для свича 2960
list_c = []     # списки для записи строк. Посредник дя ДФ
list_m = []
# пустые ДатаФреймы для маршрутизаторов
#df_cr_r0 = pd.DataFrame()
#df_mr_r0 = pd.DataFrame()
time_deco = etd()
# ------------ test ---------
ip = '10.71.220.175'
commun = 'public'
oid = ('.1.3.6.1.2.1.1.5.0')
# ---------------- SNMP --------------------
ios_ver_oid="1.3.6.1.4.1.9.9.25.1.1.1.2.7" #IOS version -------------------- ne sw
ios_ver_0 = "iso.3.6.1.2.1.1.1.0"

ios_ver_2 = "iso.3.6.1.2.1.1.1.0" # - RouterOS RB3011UiAS
ios_ver_1 = "iso.0.8802.1.1.2.1.3.4.0" # на микротах хорошо работает
cr_chassis_oid="1.3.6.1.2.1.47.1.1.1.1.2.1" # хардвэр
routeros_ver_oid="1.3.6.1.4.1.14988.1.1.4.4.0" #Что-то с ним было не так - МИКРОТЫ
routeros_oid="1.3.6.1.2.1.47.1.1.1.1.2.65536" # тоже МИКРОТЫ 
# --- SW ---
c2960hw_oid="1.3.6.1.2.1.47.1.1.1.1.13.1001" #2960 HW OID
sf300hw_oid="1.3.6.1.4.1.9.6.1.101.217.1.4.0.1" #300/350 HW oid
# РАБОТАЕТ
# Читаем файлы с данными RO из ПРТГ и заносим в ДатаФрейм для сортировки
def load_raw_from_ro():
    try:
        router_prtg = pd.read_csv("data/router_raw_prtg.csv")
        return (router_prtg[["Device","Host"]])
        #return (router_prtg[["Host"]]) # УБРАТЬ
        print ("Выгрузка данных из CSV_routers в ДатаФрейм произошла успешно! ")
        logs.log_write("Выгрузка данных из CSV_routers в ДатаФрейм произошла успешно! ")
    except Exception as e:
        print("Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg + str(e))
        logs.log_write(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg + str(e))
        
# РАБОТАЕТ
# Читаем файлы с данными SW из ПРТГ и заносим в ДатаФрейм для сортировки
def load_raw_from_sw():
    try:
        switch_prtg = pd.read_csv("data/switch_raw_prtg.csv")
        return (switch_prtg[["Device","Host"]])
        print (switch_prtg[["Device","Host"]])
        logs.log_write("Выгрузка данных из CSV_routers в ДатаФрейм произошла успешно! ")
    except Exception as e:
        print(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg + str(e))
        logs.log_write(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg + str(e))
        

# РАБОЧАЯ версия. Актуальная ДЛЯ РОУТЕРОВ
# ---------- Проба записи всех данных в ДатаФрейм, чтобы легче сортировать         
def _test_to_df_csv_ro(str1, device, address):
    # объявляем эти переменные глобальными, чтобы можно было присваивать им значения
    # из локальной области, как в процедуре
    global list_c 
    global list_m 
    try:
        if str1.startswith('None'):
            logs.trouble_write(device, 'Нет такого объекта ')
        elif (str1.startswith('No Such Object')):
            dev_os = 'routeros'
            ou = _str_cut_ou(device)             # вырезаем ОУ
            role = _str_cut_rrole(device)        # вырезаем роль
            os_ver =  (snmp.get_snmp(commun, address, routeros_ver_oid))    # -------- TEST
            mikr_hw = snmp.get_snmp(commun, address, routeros_oid) # --------
            hw_ver = (_string_cut_mikr_hw(mikr_hw) + '\n')   # --------
            list_m += [[device, address, dev_os, str(ou), hw_ver, 'Mikrotic' , os_ver, role]]
        elif (str1.lower().startswith('cisco')):
            dev_os = 'ios'
            ou = _str_cut_ou(device)             # вырезаем ОУ
            role = _str_cut_rrole(device)        # вырезаем роль        
            os_ver = (_string_cut_cisco_os(snmp.get_snmp(commun, address, ios_ver_0)))
            hw_ver = (_string_cut_cisco_hw(str1, 'chassis') + '\n')
            list_c += [[device, address, dev_os, str(ou), hw_ver, 'Cisco' , os_ver, role]]
        else:
            dev_os = 'ios'
            ou = _str_cut_ou(device)             # вырезаем ОУ
            role = _str_cut_rrole(device)        # вырезаем роль  
            os_ver = (_string_cut_cisco_os(snmp.get_snmp(commun, address, ios_ver_0)))
            hw_ver = (str1[0:4] + '\n')
            list_c += [[device, address, dev_os, str(ou), hw_ver, 'Cisco' , os_ver, role]]
    except Exception as e:  
        print (str(e) + " + test_to_df_csv_ro")

# РАБОЧАЯ версия. Актуальная ДЛЯ КОММУТАТОРОВ        
def _test_to_df_csv_sw(str1, device, address):
    # объявляем эти переменные глобальными, чтобы можно было присваивать им значения
    # из локальной области, как в процедуре
    global list_sw2960 
    global list_sw300
    try:
        if str1.lower().startswith('none'):
            logs.trouble_write(device, 'Нет такого объекта ')
        elif (str1.startswith('No Such')):
            ou = str_cut_ou(device)             # вырезаем ОУ       
            hw_ver = (_str_cut_sf300(snmp.get_snmp(commun, address, sf300hw_oid)))
            list_sw300 += [[device, address, str(ou), hw_ver]]
        else:
            ou = str_cut_ou(device)             # вырезаем ОУ 
            hw_ver = (_str_cut_c2960(str1))
            list_sw2960 += [[device, address, str(ou), hw_ver]]
    except Exception as e:  
        print (str(e) + " + test_to_df_csv_sw")

# РАБОЧАЯ версия. Актуальная
# ----- обрезает строку и удаляет всё лишнее для CISCO ------ РАБОТАЕТ
def _string_cut_cisco_hw(str_to_cut, str_to_find):
    str0 = str_to_cut.replace(' ','')
    return (str(str0[5:(str0.lower().rfind(str_to_find))]))
# РАБОЧАЯ версия. Актуальная    
def _string_cut_cisco_os(str_to_cut):
    return (str(str_to_cut[(str_to_cut.lower().rfind('version')) + 8:(str_to_cut.lower().rfind(', release'))]))
# РАБОЧАЯ версия. Актуальная
# ----- обрезает строку и удаляет всё лишнее для Mikrot ------ РАБОТАЕТ
def _string_cut_mikr_hw(str_to_cut):
    return (str_to_cut.split(' ')[-1])
# РАБОЧАЯ версия. Актуальная    
# ----- получаем ОУ и Роль роутера    
def _str_cut_ou(str_to_cut):
    return (str_to_cut.split('_')[0])
# РАБОЧАЯ версия. Актуальная    
def _str_cut_rrole(str_to_cut):
    return (str_to_cut.split('_')[-1])
# ----- Обрезаем инфу для свичей
def _str_cut_sf300(str_to_cut):
    return (str_to_cut[0:5])
def _str_cut_c2960(str_to_cut):
    return (str_to_cut[4:9])
# -------------------------------------- Запуск SNMP ---------------------------

# ------------------------- ЗАПРОС SNMP через Subprocess -------------------------------
#res = sp.run(["snmpwalk", "-v2c", '-cpublic', '10.71.220.175', '1', '>', '/home/7122110/snmpwalk_1.txt'])
#res = sp.run(["snmpwalk", "-v2c", '-cpublic', '10.71.220.175', '.1.3.6.1.2.1.1.5.0', '1', '>', '/home/7122110/snmpwalk_1.txt'])
#sp.run(['>', res, '1', '/home/7122110/snmpwalk_1.txt'])
#res = snmp.get_subproc('10.71.220.175', '.1.3.6.1.2.1.1.5.0')
#sp.run([ > , res, '1', '/home/7122110/snmpwalk_1.txt'])

#----------------------------------------------------------------------------
# ----------- ОБЩЕЕ время выполнения всех запросов

# АКТУАЛЬНАЯ, РАБОТАЕТ    
# -------------- последняя версия - имя устройства и ип адрес    
def show_total_time_DevHost(df_in):
    now = dt.now()
    df_r0 = df_in
    print("Время начала RO: " + str(now.strftime("%X")))   
    logs.log_write("Время начала : " + str(now.strftime("%X"))) 
    for i, (Device, Host) in df_r0[["Device","Host"]].iterrows():
        _test_to_df_csv_ro((str(snmp.get_snmp(commun, Host, cr_chassis_oid))), Device, Host)    # проба через ДФ в ЦСВ
        #check_chassis_DevHost((str(snmp.get_snmp(commun, Host, cr_chassis_oid))), Device, Host)
        #check_chassis_DevHost((str(snmp.get_subproc(Host, cr_chassis_oid))), Device, Host)
    print("Выполнено за : " + str(dt.now() - now))
    logs.log_write("Выполнено за : " + str(dt.now() - now))
    
# АКТУАЛЬНАЯ, РАБОТАЕТ for SWITCHES   
@time_deco 
def show_total_time_DevHost_sw(df_in):
    now = dt.now()
    df_r0 = df_in
    print("Время начала SW: " + str(now.strftime("%X")))   
    logs.log_write("Время начала : " + str(now.strftime("%X"))) 
    for i, (Device, Host) in df_r0[["Device","Host"]].iterrows():
        _test_to_df_csv_sw((str(snmp.get_snmp(commun, Host, c2960hw_oid))), Device, Host)    # проба через ДФ в ЦСВ
        #check_chassis_DevHost((str(snmp.get_snmp(commun, Host, cr_chassis_oid))), Device, Host)
        #check_chassis_DevHost((str(snmp.get_subproc(Host, cr_chassis_oid))), Device, Host)
    print("Выполнено за : " + str(dt.now() - now))
    logs.log_write("Выполнено за : " + str(dt.now() - now))

time_deco = etd()

# ВЫГРУЗКА данных из ЦСВ в указанный YML
@time_deco
def csv_to_yml(df_out):
    df = df_out
    now = dt.now()
    #print (df)
    for i, (device, address, dev_os, ou, hwid , hw_ver, os_ver, role) in df[['host','address', 'dev_os', 'ou', 'hwid' , 'hw_ver', 'os_ver', 'role']].iterrows():
        logs.cr_yml(device, address, dev_os, ou, hwid , hw_ver, os_ver, role)
        logs.log_write("ВЫГРУЗКА в YML Выполненa за : " + str(dt.now() - now))
    
    
    
# ------------------------------------- ВЫПОЛНЕНИЕ ПРОЦЕДУР и ФУНКЦИЙ.     

