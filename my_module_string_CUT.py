import my_module_get_snmp as snmp 


# ----------------------------- ТЕСТОВЫЕ переменные ---------------------------
str1 = "Cisco IOS Software, 2800 Software (C2800NM-ADVIPSERVICESK9-M), Version 15.0(1)M7, RELEASE SOFTWARE (fc2)"

str2 = "CISCO2911R/K9 chassis, Hw Serial#: JTV2114TKYH, Hw Revision: 1.0"
str3 = "3845 chassis"
str4 = "Cisco ISR4331 Chassis"
str5 = 'chassis'
str6 = 'cisco'

str7 = "Cisco IOS Software, ISR Software (X86_64_LINUX_IOSD-UNIVERSALK9-M), Version 15.5(2)S4, RELEASE SOFTWARE (fc1)"
str8 = "Cisco IOS Software, C2900 Software (C2900-UNIVERSALK9-M), Version 15.5(3)M5, RELEASE SOFTWARE (fc1)"

mikr1 = "RouterOS 6.42.12 (long-term) on RB750r2"
mikr2 = "RouterOS 6.42.11 (long-term) on RB3011UiAS"
# ---------------------- ПРОВЕРКА начала строки -----------------------
def check_ios_ver(str1):
    if str1.lower().startswith('cisco'):
        print ('Cisco')
        
    elif (str1.startswith('No Such Object')):
        print ('Microtic')
    elif (str1.startswith('None')):
        print ('None')

# ПЕРЕНЁС в другой модуль с изменениями
def check_chassis(str1):
    if str1.startswith('None'):
        print ('Нет такого объекта')
    elif (str1.startswith('No Such Object')):
        print ('Microtic')
        string_cut_mikr_hw(mikr_hw)                                              # --------
    elif (str1.lower().startswith('cisco')):
        print ('Cisco')
        string_cut_cisco_hw(str1, 'chassis')
    else:
        print ('Cisco')
        print (str1[0:4])

# --------------------------- РАБОТА с ФАЙЛОМ -----------------------
def write_to_file(str1):
    with open('logs/cisco.txt','a', encoding = 'utf8') as f:
        try:
            f.write(str1 + '\n')
            print ("Запись в файл прошла успешно!")
        except Exception as e:
            print("Запись не удалась " + str(e))
            
            
def write_to(string_to_write):
    try:
        f = open('logs/snmpwalk_cisco_os.txt', 'a', encoding = 'utf8')
        f.write(string_to_write  + '\n')
    except Exception as e:
        print('Не удалось записать : ' + str(e))
    finally:
        f.close

# ----- обрезает строку и удаляет всё лишнее для CISCO HW Version ------ РАБОТАЕТ
def string_cut_cisco_hw(str_to_cut, str_to_find):
    str0 = str_to_cut.replace(' ','')
    return (str(str0[5:(str0.lower().rfind(str_to_find))]))
    
# ----- обрезает строку и удаляет всё лишнее для CISCO OS Version ------ РАБОТАЕТ
def string_cut_cisco_os(str_to_cut):
    return (str(str_to_cut[(str_to_cut.lower().rfind('version')) + 8:(str_to_cut.lower().rfind(','))]))

# ----- обрезает строку и удаляет всё лишнее для Mikrot ------ РАБОТАЕТ
def string_cut_mikr_hw(str_to_cut):
    return (str_to_cut.split(' ')[-1])

# ----------------------------------------------------------------------


# --------------------------- ! RUN ! ----------------------------------

