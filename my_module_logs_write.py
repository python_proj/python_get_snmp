#Модуль для записи логов
import time
import os 

#class Logs:

# --------- ЗАПИСЬ логов -------------------------------------------------------------------------------------    
def log_write(string_to_write):
    try:
        f = open('logs/logs.txt', 'a', encoding = 'utf8')
        f.write(str(time.strftime("In %X %d %B, %Y", time.localtime())) + ' | ' + string_to_write  + '\n')
    except Exception as e:
        print(str(time.strftime("In %X %d %B, %Y", time.localtime())) + ' | ' + 'Не удалось записать Лог: ' +  str(e))
    finally:
        f.close

# --------- ЗАПИСЬ в файл Циско роутеров
def cr_write(bu_name, os_ver, hw_ver):
    try:
        with open('logs/cr.txt', 'a', encoding = 'utf8') as f:
            f.write(f"{bu_name} \n{os_ver} \n{hw_ver} \n\n ")
    except Exception as e:
        print(str(time.strftime("In %X %d %B, %Y", time.localtime())) + ' | ' + 'Не удалось записать Цискарь: ' +  str(e))

# --------- ЗАПИСЬ в файл Микрот роутеров
def mr_write(bu_name, os_ver, hw_ver):
    try:
        with open('logs/mr.txt', 'a', encoding = 'utf8') as f:
            f.write(f'{bu_name}\n{os_ver}\n{hw_ver}\n\n')
    except Exception as e:
        print('Не удалось записать Микрот: '+ ' | ' + str(time.strftime("In %X %d %B, %Y", time.localtime())) + str(e))

# --------- ЗАПИСЬ в файл проблемных роутеров
def trouble_write(bu_name, str1):
    try:
        with open('logs/unknown_r.txt', 'a', encoding = 'utf8') as f:
            f.write(f'{bu_name}\n{str1}\n\n')
    except Exception as e:
        print('Не удалось записать problem R0: '+ ' | ' + str(time.strftime("In %X %d %B, %Y", time.localtime())) + str(e))
                
# ---------- создание файла ххх.yml и заполнение его по шаблону
#
# ---------- шаблон заполнения
def _insert_data(device, host, ios, ou_id, hwid, devver, osver, router_role):
    try:
        with open(f'host_vars/{device}/{device}.yml', 'w', encoding="utf8") as f: 
            f.write(f'ansible_host:  {host}\n' +
                    'ansible_connection: network_cli\n' +
                    f'ansible_network_os:  {ios}\n' +
                    f'ou_id: {ou_id}\n' +
                    f'hwid: {hwid}' +
                    f'devver: {devver}\n' +
                    f'osver: {osver} \n' +
                    f'router_role: {router_role}\n')
    except Exception as e:
        log_write(f"Не удалось записать файл с именем  {device}  " + str(e))

# ------- создаём директорию по имени устройства, если её нет.
def cr_yml(device, host, ios, ou_id, hwid, devver, osver, router_role):
    try:
        if not os.path.isdir(f'host_vars/{device}'):
            os.makedirs(f'host_vars/{device}')
            _insert_data(device, host, ios, ou_id, hwid, devver, osver, router_role)
            log_write(f"папка  {device} создана и файл {device}.yml записан" )
        else:
            _insert_data(device, host, ios, ou_id, hwid, devver, osver, router_role)
            log_write(f"файл  {device}.yml записан")
    except Exception as e:
        log_write(f"Не удалось создать папку или записать файл с именем  {device} :  " + str(e))

        
# ----- тест последней процедуры    -     
#cr_yml('711111', '10.71.2.255', 'ios', '712', 'CISCO2911R/K9', 'cisco', '15.0(1)M7', 'R0')
#cr_write('bu_name', 'os_ver', 'hw_ver')