import os
import time
from datetime import datetime as dt
import shutil
import wget
import pathlib
import my_module_logs_write as logs
import pandas as pd

# ------------------------| импорт моих модулей |-----------------------
import my_module_load_from_file as load 
import my_module_group_BU as group 


#------------------------------------| СОЗДАЁМ ПЕРЕМЕННЫЕ |------------------------
#
# Пути к файлам
cur_dir = os.getcwd()
data_dir = cur_dir + '/data'
old_data_dir = cur_dir + '/old_data'
log_dir = cur_dir + '/logs'
tmp_dir = cur_dir + '/tmp'
#
# -------------------------| переменные из других модулей |-------------
df_r0 = pd.DataFrame() # пустой фрейм для списка роутером
df_s0 = pd.DataFrame() # пустой фрейм для списка коммутаторов
list_c = []     # списки для записи строк. Посредник дя ДФ
list_m = []
list_sw300= []      # список для свича 300
list_sw2960 = []    # список для свича 2960


# Временные файлы
"""
router_raw_prtg = cur_dir + "/" + "router_raw_prtg.csv"
switch_raw_prtg = cur_dir + "/" + "switch_raw_prtg.csv"
router_prtg = cur_dir + "/" + "router_prtg.csv"
switch_prtg = cur_dir + "/" + "switch_prtg.csv"
"""
router_raw_prtg = 'data' + "/" + "router_raw_prtg.csv"
switch_raw_prtg = 'data' + "/" + "switch_raw_prtg.csv"

# Файлы шаблонов, логов и т.д.
tmp_sw_300 = tmp_dir + "/" + "df_sw_300.csv"
tmp_sw_2960 = tmp_dir + "/" + "df_sw_2960.csv"
cr_template = tmp_dir + "/" + "cr.template"
mr_template = tmp_dir + "/" + "mr.template"

# Файлы, в которые будут записываться результаты
cr_file = data_dir  + "/" + "cr.inventory" #there we will put Cisco routers
mr_file = data_dir  + "/" + "mr.inventory" #there we will put Mikrotik routers
cs2900_file = data_dir  + "/" + "2960-cs.txt"  
cs3xx_file = data_dir  + "/" + "300-cs.txt" #there we will put sf3XXs
no_snmp_file = data_dir  + "/" + "no_snmp.txt" #There we will put hosts which does not respond for snmp. Either offline or missconfigured
unknown_file = data_dir  + "/" + "unknown.txt" #wtf is that?


# Другое
cur_time = str(time.strftime("In %X %d %B, %Y", time.localtime()))
now = dt.now()

# Рисуем красивую полоску для отделения каждого выполнения
logs.log_write("------------------------------------------------------------------------------------------")

#-----------------------------------------| ЗАДАЁМ функции |--------------------
#
# Создаём папки, если они не существуют
try:
    os.mkdir(data_dir)
    os.mkdir(old_data_dir)
    os.mkdir(log_dir)
    logs.log_write("Все директории успешно созданы")
except OSError:
    print("Создать директорию %s не удалось, потому что она уже есть" % data_dir)
    print("Создать директорию %s не удалос, потому что она уже естьь" % old_data_dir)
    print("Создать директорию %s не удалось, потому что она уже есть" % log_dir)
    logs.log_write("Создать директорию %s не удалось, потому что она уже есть"  % data_dir)
    logs.log_write("Создать директорию %s не удалось, потому что она уже есть"  % old_data_dir)
    logs.log_write("Создать директорию %s не удалось, потому что она уже есть" % log_dir)
else:
    print("Успешно созданы папки")
    logs.log_write("Все директории успешно созданы")
    
print("Execution started at   " + cur_time)
# пишем лог
logs.log_write("Execution started at   " + cur_time)

# Бэкапим предыдущие результаты, если они существуют
#
# 1 - СНАЧАЛА запускаем эту функцию
def backup_last():
    try:
        if pathlib.Path.exists(cs2900_file):
            shutil.copy(cs2900_file, data_dir  + "/" + "old_2960-cs.text") 
        if os.path.exists(cs3xx_file):                
            shutil.copy(cs3xx_file, data_dir  + "/" + "old_300-cs.text") 
        if os.path.exists(no_snmp_file):               
            shutil.copy(no_snmp_file, data_dir  + "/" + "old_no_snmp.text") 
        if os.path.exists(un_known_file):
            shutil.copy(un_known_file, data_dir  + "/" + "old_unknown.text") 
        if os.path.exists(mr_file):   
            shutil.copy(mr_file, data_dir  + "/" + "old_mikrotik.text")
        if os.path.exists(cr_file):    
            shutil.copy(cr_file, data_dir  + "/" + "old_cr.text") 
        logs.log_write("Бэкап предыдущих версий завершился успехом   ")
    except Exception as e:
        print ("Бэкап предыдущих результатов не удался потому что:  " + str(e))
        logs.log_write("Бэкап предыдущих результатов не удался потому что:  " + str(e))
    finally:   
        print ("успешно отбэкапили")
        logs.log_write("успешно отбэкапили")
        
# ВТОРОЙ вариант бэкапа всех файлов
def backup2():
    try:
        shutil.copytree(data_dir, old_data_dir)
        shutil.copytree(tmp_dir, old_data_dir)
    except Exception as e:
        print ("2 Бэкап предыдущих результатов не удался потому что:  " + str(e))
        logs.log_write("2 Бэкап предыдущих результатов не удался потому что:  " + str(e))
    finally:   
        print ("2 успешно отбэкапили")
        logs.log_write("2 успешно отбэкапили")

# выкачиваем данные с ПРТГ в цсв для роутеров
#
# 2 - ПОТОМ эту
def get_raw_routers():
    if not pathlib.Path(router_raw_prtg).exists():
        try:
            wget.download("http://mon-d-001.kifr-ru.local/api/table.csv?content=devices&output=csvtable&columns=device,host&filter_device=@sub(_R1)&filter_device=@sub(_R0)&filter_device=@sub(_R2)&filter_device=@sub(_RX)&username=readonly_api&passhash=3462474503&count=*", router_raw_prtg)
            print ("Выгрузка данных из ПРТГ произошла успешно! " + router_raw_prtg)
            logs.log_write("Выгрузка данных из ПРТГ произошла успешно! " + router_raw_prtg)
        except e:
            print(str(e) + "Выгрузка из ПРТГ не удалась (( " + router_raw_prtg)
            logs.log_write(str(e) + "Выгрузка из ПРТГ не удалась (( " + router_raw_prtg)
    else:
        os.remove(router_raw_prtg)
        print ("существующий файл удалён " + router_raw_prtg)
        logs.log_write("существующий файл удалён " + router_raw_prtg)
        wget.download("http://mon-d-001.kifr-ru.local/api/table.csv?content=devices&output=csvtable&columns=device,host&filter_device=@sub(_R1)&filter_device=@sub(_R0)&filter_device=@sub(_R2)&filter_device=@sub(_RX)&username=readonly_api&passhash=3462474503&count=*", router_raw_prtg)            
        print ("Выгрузка данных из ПРТГ произошла успешно! " + router_raw_prtg)            
        logs.log_write("Выгрузка данных из ПРТГ произошла успешно! " + router_raw_prtg)
        


# выкачиваем данные с ПРТГ в цсв для switches
#
# 3 - ЗАТЕМ эту
def get_raw_switches():
    if not pathlib.Path(switch_raw_prtg).exists():
        try:
            wget.download("http://mon-d-001.kifr-ru.local/api/table.csv?content=devices&output=csvtable&columns=device,host&filter_device=@sub(_SW)&username=readonly_api&passhash=3462474503&count=*", switch_raw_prtg)
            print ("Выгрузка данных из ПРТГ произошла успешно! " + switch_raw_prtg)
            logs.log_write("Выгрузка данных из ПРТГ произошла успешно! " + switch_raw_prtg)
        except e:
            print(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg)
            logs.log_write(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg)
    else:
        os.remove(switch_raw_prtg)
        print (switch_raw_prtg + " существующий файл удалён")
        logs.log_write(switch_raw_prtg + " существующий файл удалён")
        wget.download("http://mon-d-001.kifr-ru.local/api/table.csv?content=devices&output=csvtable&columns=device,host&filter_device=@sub(_SW)&username=readonly_api&passhash=3462474503&count=*", switch_raw_prtg)            
        print (switch_raw_prtg + " Выгрузка данных из ПРТГ произошла успешно!")            
        logs.log_write(switch_raw_prtg + " Выгрузка данных из ПРТГ произошла успешно!")

# Читаем файлы с данными RO из ПРТГ и заносим в ДатаФрейм для сортировки
def load_raw_from_ro():
    try:
        router_prtg = pd.read_csv("data/router_raw_prtg.csv")
        print (router_prtg[["Device","Host"]])
        logs.log_write("Выгрузка данных из CSV_routers в ДатаФрейм произошла успешно! ")
    except e:
        print(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg)
        logs.log_write(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg)

# Читаем файлы с данными SW из ПРТГ и заносим в ДатаФрейм для сортировки
def load_raw_from_sw():
    try:
        router_prtg = pd.read_csv("data/switch_raw_prtg.csv")
        print (router_prtg[["Device","Host"]])
        logs.log_write("Выгрузка данных из CSV_routers в ДатаФрейм произошла успешно! ")
    except e:
        print(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg)
        logs.log_write(str(e) + "Выгрузка из ПРТГ не удалась (( " + switch_raw_prtg)




# ----------------------------------------------------| ЗАПУСКАЕМ функции |-----------------
#
#
print ("ВРЕМЯ НАЧАЛА ВСЕЙ КОНСТРУКЦИИ : "  + str(now.strftime("%X")))
logs.log_write("ВРЕМЯ НАЧАЛА ВСЕЙ КОНСТРУКЦИИ : "  + str(now.strftime("%X")) + ' ')
# 1
backup_last()
#backup2()

# 2
get_raw_routers()
time.sleep(2)
print ("ПИРИФ, нащальника )) поспали, едем дальше. ")
# 3
get_raw_switches()
print ("Выполнена выгрузка из ПРТГ за : " + str(dt.now() - now))

# ----------------------------------------------------| ЗАПУСКАЕМ функции из модулей |-----------------

# СНАЧАЛА по МАРШРУТИЗАТОРАМ
print ("ЗАГРУЗКА НАЧАТА для ДФ из списка маршрутизатов")
df_r0 = load.load_raw_from_ro()         # -------- по маршрутизаторам
#print (df_r0)     # TEST PROD
print ("ЗАГРУЗКА ВЫПОЛНЕНА")

print ("НАЧАЛ выполнение запросов SNMP")
load.show_total_time_DevHost(df_r0)         # --------- R
time.sleep(1)
#print (list_c)     # TEST PROD
print ("SNMP запросы выполнены и данные сохранены в список")

print ('сначала выгружаем с ДФ данные из списка, а потом сохраняем в ЦСВ')   
df_cr_r0 = pd.DataFrame(load.list_c, columns=['host','address', 'dev_os', 'ou', 'hwid' , 'hw_ver', 'os_ver', 'role'])
#print(df_cr_r0)
df_cr_r0.to_csv('tmp/df_cr_r0.csv', sep=';', index=False)
df_mr_r0 = pd.DataFrame(load.list_m, columns=['host','address', 'dev_os', 'ou', 'hwid' , 'hw_ver', 'os_ver', 'role']) # работает как надо. 
#print(df_mr_r0)
df_mr_r0.to_csv('tmp/df_mr_r0.csv', sep=';', index=False)           # работает как надо. 
print ('ВЫПОЛНЕНО') 

# запуск выгрузки в YML 
print("Выгрузка в YML")   
load.csv_to_yml(df_cr_r0)
load.csv_to_yml(df_mr_r0)
print ('ВЫПОЛНЕНО')

print ("Запись результатов в файлы TEMPLATE и сортировка")
#group.open_write()
# ------------ Вся связка работает хорошо 
# сначала копируем шапку и перезаписываем файл
with open(cr_template, 'r') as source, open(cr_file, 'w') as dest:
    data = source.read()
    dest.write(data)
with open(mr_template, 'r') as source, open(mr_file, 'w') as dest:
    data = source.read()
    dest.write(data)   
# затем запускаем все процедуры по порядку
group.cr_all_bu(df_cr_r0, cr_file)
group.mr_all_bu(df_mr_r0, mr_file)
group.all_cr_to_group(df_cr_r0, cr_file)
group.all_mr_to_group(df_mr_r0, mr_file)
print ('ВЫПОЛНЕНО')

# ТЕПЕРЬ ПО КОММУТАТОРАМ
df_s0 = load.load_raw_from_sw()         # -------- по коммутаторам
load.show_total_time_DevHost_sw(df_s0)         # --------- SW
print ('сначала выгружаем с ДФ данные из списка, а потом сохраняем в ЦСВ SWITCHES')   
df_sw_300 = pd.DataFrame(load.list_sw300, columns=['host','address', 'ou', 'hw_ver']) # работает как надо. 
df_sw_300.to_csv(tmp_sw_300, sep=';', index=False)           # работает как надо. 
df_sw_2960 = pd.DataFrame(load.list_sw2960, columns=['host','address', 'ou', 'hw_ver'])
df_sw_2960.to_csv(tmp_sw_2960, sep=';', index=False)
print ('ВЫПОЛНЕНО') 
# Начата сортировка по группам и запись SW в файлы
group.sw300_all_bu(df_sw_300, cs3xx_file)
group.sw2960_all_bu(df_sw_2960, cs2900_file)
# Завершено успешно!

# ------------------------------- КОНЕЦ -----------------------

print ("ВРЕМЯ ЗАВЕРШЕНИЯ ВСЕЙ КОНСТРУКЦИИ : "  + str(dt.now()))
logs.log_write("ВРЕМЯ ЗАВЕРШЕНИЯ ВСЕЙ КОНСТРУКЦИИ : "  + str(dt.now()))
print (' ')
print ("ОБЩЕЕ ВРЕМЯ ВЫПОЛНЕНИЯ ВСЕЙ КОНСТРУКЦИИ : "  + str(dt.now() - now))
logs.log_write("ОБЩЕЕ ВРЕМЯ ВЫПОЛНЕНИЯ ВСЕЙ КОНСТРУКЦИИ : "  + str(dt.now() - now))

