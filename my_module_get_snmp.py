import pysnmp
import os
from pysnmp.hlapi import *
#from ipaddress import * УБРАТЬ
from pprint import pprint
from datetime import datetime as dt
import subprocess as sp
import pandas as pd


# ----------------- ПЕРЕМЕННЫЕ------------------


ip = '10.71.220.175'
commun = 'public'
oid = ('.1.3.6.1.2.1.1.5.0')

ios_ver_oid="1.3.6.1.4.1.9.9.25.1.1.1.2.7" #IOS version
cr_chassis_oid="1.3.6.1.2.1.47.1.1.1.1.2.1" #
routeros_ver_oid="1.3.6.1.4.1.14988.1.1.4.4.0" #Что-то с ним было не так - МИКРОТЫ
routeros_oid="1.3.6.1.2.1.47.1.1.1.1.2.65536" # тоже МИКРОТЫ 

#df1 = pd.DataFrame()

#df1 = ["10.0.7.252" , "10.0.7.253", "10.0.8.252" , "10.50.1.252", "10.50.1.253" , "10.50.6.252", "10.50.6.253", "10.50.7.252", "10.50.7.253", "10.50.9.252"]

#---------------------------1я версия запроса SNMP - на две функции-----------------------------
#----------------------------РАБОТАЕТ ВСЁ---------
def deco2(func):            #----время выполнения КАЖДОГО запроса. OLD
    def wrap():
        now = dt.now()
        pprint("Время начала : " + str(now.strftime("%X")))
        func()
        pprint("Выполнено за : " + str(dt.now() - now))
    return wrap

def _get_name(comm, ip, oid):
    
    return (getCmd(SnmpEngine(),
                   CommunityData(commun),
                   UdpTransportTarget((ip, 161)),
                   ContextData(),
                   ObjectType(ObjectIdentity(oid))))
    
def _snmp_get_text(comm, ip, oid):    
    errorIndication, errorStatus, errorIndex, varBinds = next(_get_name(comm, ip, oid))
    for name, val in varBinds:
        return ( val.prettyPrint())
        
#---------------------------2я версия запроса SNMP - в одну функцию-----------------------------
#---------------------------РАБОТАЕТ
def deco3(func):                        # OLD
#    now = dt.now()
#    pprint("Время начала : " + str(now.strftime("%X")))
    def wrap():    
        func()
#    pprint("Выполнено за : " + str(dt.now() - now))    
    return wrap
    
# ОТЛИЧНАЯ РАБОЧАЯ ВЕРСИЯ
def get_snmp(get_comm, get_ip, get_oid):
    try:
        obj_type = [ObjectType(ObjectIdentity(get_oid))]
        errorIndication, errorStatus, errorIndex, varBinds = next(
                getCmd(SnmpEngine(),
                    CommunityData(get_comm),
                    UdpTransportTarget((get_ip, 161)),
                    ContextData() ,
                    *obj_type
                    )
            )
        for name, val in varBinds:
            return (val.prettyPrint())

    except Exception as e:
        print (e)
             

#sys_name = (snmp_get_text())
    
#@deco2
#def show_time_2():
#    print (str(snmp_get_text(commun, ip ,oid)))
    
#show_time_2()

#pprint(df1)

'''for i in df1:
    @deco2
    def show_time_2():
        print (str(snmp_get_text(commun, i, routeros_ver_oid)))
        print (i)

    show_time_2()'''

# ----------- ОБЩЕЕ время выполнения всех запросов
def _show_total_time():
    now = dt.now()
    pprint("Время начала : " + str(now.strftime("%X")))    
    for i in df1:
        print (str(get_snmp(commun, i, cr_chassis_oid)))

    pprint("Выполнено за : " + str(dt.now() - now))
    
#show_total_time()


def _get_subproc(ip, oid):
    now = dt.now()
    pprint("Время начала : " + str(now.strftime("%X"))) 
    return str(sp.run(["snmpwalk", "-v2c", '-cpublic', ip, oid]))
    pprint("Выполнено за : " + str(dt.now() - now))
#res = sp.run(["snmpwalk", "-v2c", '-cpublic', '10.71.220.175', '.1.3.6.1.2.1.1.5.0', '1', '>', '/home/7122110/snmpwalk_1.txt'])
#print (df1)


