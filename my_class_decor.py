# мой класс декоратор для расчёта времени выполнения модулей и функций

from datetime import datetime as dt 

class elapsed_time_deco:
    def __init__(self):
        pass

    def __call__(self, func):
        def wrap(*args):
            begin = dt.now()
            print ("DECO. декоратор дя функции:  ", func.__name__)
            print(f'Время начала выполнения  {begin}')
            func(*args)
            end = dt.now() - begin
            print (f'Время завершения выполнения  {end}')
            print ('Общее время выполнения:  ' + str(dt.now()) )
            return func
        return wrap

'''deco1 = elapsed_time_deco()

@deco1
def count(num):
    print (str(((num * 12 - 1024)/ 45 + 434) * 6))

count(21)'''