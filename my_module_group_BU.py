# --------------------------------------------------------------------------------------
# модуль с функциями для группировки по БЮ и по Роли (R0 или R1) из ДатаФрейма
# или ЦСВ,  и последующей выгрузки
# списка маршрутизаторов CISCO и MICROTIC в ТХТ - файл
# --------------------------------------------------------------------------------------

import time
import pandas as pd

# -------------------------------------------------- ПЕРЕМЕННЫЕ 

#df_cr = pd.read_csv('tmp/df_cr_r0.csv')     #  дф с инфой из цсв


# --------------------------------------------------- ФУНКЦИИ И ПРОЦЕДУРЫ

# запись в ТХТ всех БЮ в группу all-cr - OLD
def all_cr_to_file():
    try:
        f = open('logs/all-cr.txt', 'w', encoding = 'utf8')
        f.write('[all-cr]\n')
        df_cr = pd.read_csv('tmp/df_cr_r0.csv', sep=";")     #  дф с инфой из цсв
        for host in df_cr['host'].tolist():
            #print (host)
            f.write('%s\n' % host)
    except Exception as e:
        print (str(e))
    finally:
        f.close

# запись в ТХТ БЮ в группу 700-cr - OLD
def cr700_to_file():
    try:
        f = open('logs/700-cr.txt', 'w', encoding = 'utf8')
        f.write('[700-cr]\n')
        df_cr = pd.read_csv('tmp/df_cr_r0.csv', sep=";")     #  дф с инфой из цсв
        for host in df_cr['host'].tolist():
            if host.startswith('700'):
                f.write('%s\n' % host)
    except Exception as e:
        print (str(e))
    finally:
        f.close

# для CISCO раскидывание по группам и выгрузка в текстовый файл
def cr_all_bu(df_in, path):
    try:
        list_r0 = []
        list_r1 = []
        list_o_r0 = []
        list_o_r1 = []
        f = open(path, 'a', encoding = 'utf8')
        f.write('\n')
        #df_cr = pd.read_csv('tmp/df_cr_r0.csv', sep=";")     #  дф с инфой из цсв
        df_cr = df_in
        for host in df_cr['host'].tolist():
            #if  (not (host.startswith('700')) or not(host.startswith('710'))):
            if (str(host[0:3]) != '700') and (str(host[0:3]) != '710'):
                if (not (host[3]=='.') and host.endswith('R0')):
                    #f.write('%s\n' % host)
                    list_r0 += str(host + '\n')
                    #print (host)
                elif (not (host[3]=='.') and host.endswith('R1')):
                    list_r1 += str(host + '\n')
                    #print(host)
                elif ((host[3]=='.') and host.endswith('R0')):
                    list_o_r0 += str(host + '\n')
                    #print(host)
                else:
                    ((host[3]=='.') and host.endswith('R1'))
                    list_o_r1 += str(host + '\n')
                    #print(host)
            else:
                print ("700 or 710 cr")
    except Exception as e:
        print (str(e))
    finally:
# запись всех всписков по порядку в файл
        f.write('[st-cr-r0]\n')
        f.writelines(list_r0 )
        f.write('\n')
        f.write('[st-cr-r1]\n')
        f.writelines(list_r1)
        f.write('\n')
        f.write('[other-cr-r0]\n')
        f.writelines(list_o_r0)
        f.write('\n')
        f.write('[other-cr-r1]\n')
        f.writelines(list_o_r1)
        f.close    


# для MICROTIC раскидывание по группам и выгрузка в текстовый файл
def mr_all_bu(df_in, path):
    try:
        list_r0 = []
        list_r1 = []
        list_o_r0 = []
        list_o_r1 = []
        f = open(path, 'a', encoding = 'utf8')
        f.write('\n')
        #df_cr = pd.read_csv('tmp/df_mr_r0.csv', sep=";")     #  дф с инфой из цсв
        df_mr = df_in
        for host in df_mr['host'].tolist():
            if (not (host[3]=='.') and host.endswith('R0')):
                #f.write('%s\n' % host)
                list_r0 += str(host + '\n')
                #print (host)
            elif (not (host[3]=='.') and host.endswith('R1')):
                list_r1 += str(host + '\n')
                #print(host)
            elif ((host[3]=='.') and host.endswith('R0')):
                list_o_r0 += str(host + '\n')
                #print(host)
            else:
                ((host[3]=='.') and host.endswith('R1'))
                list_o_r1 += str(host + '\n')
                #print(host)
    except Exception as e:
        print (str(e))
    finally:
# запись всех всписков по порядку в файл
        f.write('[st-mr-r0]\n')
        f.writelines(list_r0 )
        f.write('\n')
        f.write('[st-mr-r1]\n')
        f.writelines(list_r1)
        f.write('\n')
        f.write('[other-mr-r0]\n')
        f.writelines(list_o_r0)
        f.write('\n')
        f.write('[other-mr-r1]\n')
        f.writelines(list_o_r1)
        f.close


# запись всех маршрутизаторов из БЮ в группу по имени БЮ
def all_cr_to_group(df_in, path):
    try:
        list_r0 = []
        list_r1 = []
        f = open(path, 'a', encoding = 'utf8')
        f.write('\n')
        #df_cr = pd.read_csv('tmp/df_cr_r0.csv', sep=';')     #  дф с инфой из цсв
        df_cr = df_in
        name_bu = '700'
        for host in df_cr['host'].tolist():
            if (str(host[0:3]) != '700') and (str(host[0:3]) != '710'):
                if (name_bu != str(host[0:3])):
                    list_r0.append('[' + str(host.split('_')[0] + 'cr]\n'))
                    list_r0 += [(host + '\n')]
                    name_bu = str(host[0:3])
                else:
                    list_r0 += [(host + '\n\n')]
            else:
                print ("700 or 710")
                
    except Exception as e:
        print (str(e))
    finally:
        f.writelines(list_r0)
        f.close
        

# запись всех маршрутизаторов из БЮ в группу по имени БЮ
# для MICROTIC раскидывание по группам и выгрузка в текстовый файл
def all_mr_to_group(df_in, path):
    try:
        list_r0 = []
        list_r1 = []
        f = open(path, 'a', encoding = 'utf8')
        f.write('\n')
        #df_cr = pd.read_csv('tmp/df_mr_r0.csv', sep=';')     #  дф с инфой из цсв
        df_mr = df_in
        name_bu = '700'
        for host in df_mr['host'].tolist():
            #name = '700'
            #print (name + '  - name')
            if (name_bu != str(host[0:3])):
                #print (name + ' 1')
                #list_r0 += [('[' + str(host.split('_')[0] + 'mr]'))]
                list_r0.append('[' + str(host.split('_')[0] + 'mr]\n'))
                #print (host)
                #print (list_r0)
                list_r0 += [(host + '\n')]
                name_bu = str(host[0:3])
            else:
                #list_r0 = str(host.split('_')[0] + 'cr')
                list_r0 += [(host + '\n\n')]
                
                #print (host + '\n')
                #name = str(host[0:3])
    except Exception as e:
        print (str(e))
    finally:
        f.writelines(list_r0)
        #return (list_r0)
        f.close

# странно отрабатывает. Не всё записывает.         
def open_write(source_c, dest_c, source_m, dest_m):
    #with open('tmp/cr.template', 'r') as source, open('logs/cr.inventory', 'w') as dest:
    try:
        #source = open('tmp/cr.template', 'r')
        #dest = open('logs/cr.inventory', 'w')
        data = source_c.read()
        dest_c.write(data)
        time.sleep(1)
        cr_all_bu()    
        #time.sleep(1)
        all_cr_to_group()   # норм 
    except Exception as e:
        print (str(e))
    finally:
        source_c.close
        dest_c.close
    #with open('tmp/mr.template', 'r') as source, open('logs/mr.inventory', 'w') as dest:
    try:
        #source = open('tmp/mr.template', 'r')
        #dest = open('logs/mr.inventory', 'w')
        data = source_m.read()
        dest_m.write(data)
        time.sleep(1)
        mr_all_bu()         # норм        
        all_mr_to_group()   # норм 
    except Exception as e:
        print (str(e))
    finally:
        source_m.close
        dest_m.close        
        

# для CISCO-SWITCHES раскидывание по группам и выгрузка в текстовый файл
def sw300_all_bu(df_in, path):
    try:
        list_sw0 = []
        #list_sw1 = []
        f = open(path, 'w', encoding = 'utf8')
        f.write('\n')
        #df_cr = pd.read_csv('tmp/df_mr_r0.csv', sep=';')     #  дф с инфой из цсв
        df_sw300 = df_in
        name_bu = '777'
        for host in df_sw300['host'].tolist():
            if (name_bu != str(host[0:3])):
                #print (name + ' 1')
                #list_r0 += [('[' + str(host.split('_')[0] + 'mr]'))]
                list_sw0.append('[' + str(host.split('_')[0] + 'mr]\n'))
                #print (host)
                #print (list_r0)
                list_sw0 += [(host + '\n')]
                name_bu = str(host[0:3])
            else:
                #list_r0 = str(host.split('_')[0] + 'cr')
                list_sw0 += [(host + '\n\n')]
                
                #print (host + '\n')
                #name = str(host[0:3])
    except Exception as e:
        print (str(e))
    finally:
        f.writelines(list_sw0)
        #return (list_r0)
        f.close 
        
def sw2960_all_bu(df_in, path):
    try:
        list_sw0 = []
        #list_sw1 = []
        f = open(path, 'a', encoding = 'utf8')
        f.write('\n')
        #df_cr = pd.read_csv('tmp/df_mr_r0.csv', sep=';')     #  дф с инфой из цсв
        df_sw29 = df_in
        name_bu = '777'
        for host in df_sw29['host'].tolist():
            if (name_bu != str(host[0:3])):
                #print (name + ' 1')
                #list_r0 += [('[' + str(host.split('_')[0] + 'mr]'))]
                list_sw0.append('[' + str(host.split('_')[0] + 'mr]\n'))
                #print (host)
                #print (list_r0)
                list_sw0 += [(host + '\n')]
                name_bu = str(host[0:3])
            else:
                #list_r0 = str(host.split('_')[0] + 'cr')
                list_sw0 += [(host + '\n\n')]
                
                #print (host + '\n')
                #name = str(host[0:3])
    except Exception as e:
        print (str(e))
    finally:
        f.writelines(list_sw0)
        #return (list_r0)
        f.close 
        
# ------------------ RUN ----------------------
# Вся связка работает хорошо 
'''with open('tmp/mr.template', 'r') as source, open('logs/cr.inventory', 'w') as dest:
    data = source.read()
    dest.write(data)
with open('tmp/mr.template', 'r') as source, open('logs/mr.inventory', 'w') as dest:
    data = source.read()
    dest.write(data)
    
cr_all_bu()
mr_all_bu()
all_cr_to_group()
all_mr_to_group()'''
#
#open_write()       # РАБОЧАЯ. ОДНА запускает все по порядку. Не очень хорошо отрабатывает

#sw300_all_bu()